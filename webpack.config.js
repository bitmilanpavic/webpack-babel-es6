const path = require('path');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'development',
    // watch: true,
    optimization:{
        // minimize: true
    },
    entry: './js/custom.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'custom.bundle.js',
        publicPath: '/build'
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [
            {
               test: /\.js$/,
               loader: 'babel-loader',
               exclude: /node_modules/  
            },
            {
                test: /\.(s*)css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1  
                        }
                    },
                    {
                        loader:'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: (loader) => [
                                autoprefixer({ browsers: [">1%", "last 20 versions"] })
                            ]
                        }
                    },
                    {
                        loader:'sass-loader'    
                    }]
                })
                  
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'url-loader?limit=8000&name=images/[name].[ext]' 
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({filename: 'style.css'})
    ]
}